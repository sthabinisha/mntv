package com.newitventure.mntvlive;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;

import com.newitventure.mntvlive.entity.GetMac;
import com.newitventure.mntvlive.entity.LinkConfig;
import com.newitventure.mntvlive.util.DownloadUtil;
import com.newitventure.mntvlive.util.UnauthorizedAccess;
import com.newitventure.mntvlive.util.common.CustomDialogManager;
import com.newitventure.mntvlive.util.common.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Niroj on 18/03/2016.
 */
public class EntryPoint extends Activity{


    private static String macAddress;
    private String version = "";
    String mac_link, channel_url, username;
    private String error_message = "";
    private String error_code = "";
    private static final String TAG = "EntryPoint";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_point);

        if (AppConfig.isDevelopment()) {
            macAddress = AppConfig.getMacAddress();
        } else {
            macAddress = GetMac.getMac(this); // Getting mac addresss
        }


        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
          Logger.printStackTrace(e);
        }


        mac_link =  LinkConfig.getString(EntryPoint.this,
                LinkConfig.MAC_EXISTS) + "?mac=" + macAddress;
        Log.d("Mac",mac_link);




        new CheckIfValidMacAddress().execute(mac_link/*
															 * getResources(
															 * )
															 * .getString
															 * ( R.
															 * string
															 * .base_url
															 * ) +
															 * "mac_exists.php?mac="
															 * +
															 * macAddress
															 */);

//        Thread timerThread = new Thread(){
//            public void run(){
//                try{
//                    sleep(3000);
//                }catch(InterruptedException e){
//                    e.printStackTrace();
//                }finally{
//                    String linkVersionCheck = LinkConfig.getString(
//                            EntryPoint.this, LinkConfig.VERSION_CHECK)
//                            + "?apk=mntv_live&version=" + version;
//
//                    Log.d("Version",linkVersionCheck);
//
//                    JSONParserVersionCheck versionCheck = new JSONParserVersionCheck(
//                            EntryPoint.this);
//
//                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
//                        versionCheck.execute(linkVersionCheck);
//                    } else {
//                        versionCheck.executeOnExecutor(
//                                AsyncTask.THREAD_POOL_EXECUTOR,
//                                linkVersionCheck);
//                    }
//
//                }
//            }
//
//        };
//        timerThread.start();
    }


    public class CheckIfValidMacAddress extends
            AsyncTask<String, String, String> {
        String url = "";

        private String macExists;

        @Override
        protected String doInBackground(String... params) {
            Log.d(TAG, params[0]);
            url = params[0];
            DownloadUtil dutil = new DownloadUtil(params[0], EntryPoint.this);
            String jsonString = dutil.downloadStringContent();
            return jsonString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // offline
            if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
                Log.d(TAG, "internet not available");
                final CustomDialogManager noInternet = new CustomDialogManager(EntryPoint.this, CustomDialogManager.ALERT);
                noInternet.build();
                noInternet.setTitle("No Internet");
                noInternet.setMessage("Please Check Your Internet Connection");
                noInternet.setPositiveButton(getString(R.string.btn_dismiss), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noInternet.dismiss();
                        finish();
                    }
                });
                noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        noInternet.dismiss();
                        startActivity(i);




                    }
                });
                noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(
                                android.provider.Settings.ACTION_SETTINGS);

                        noInternet.dismiss();
                        startActivity(intent);

                    }
                });
                noInternet.show();

                // server not rechable
            } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                Log.d(TAG, "server_error");
                Intent i = new Intent(EntryPoint.this, UnauthorizedAccess.class);
                i.putExtra("error_code", "");
                i.putExtra("error_message",
                        getString(R.string.server_unreachable_message));
                i.putExtra("username", username);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

            } else {
                try {
                    JSONObject jObj = new JSONObject(result);
                    macExists = jObj.getString("mac_exists");
                    if (macExists.equals("no")) {
                        System.out.println(macExists);
                        System.out.println("no");

                        JSONObject ob1 = jObj.getJSONObject("message");
                        error_message = ob1.getString("message_body");
                        Intent i1 = new Intent(EntryPoint.this,
                                UnauthorizedAccess.class);
                        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i1.putExtra("error_code", error_code);
                        i1.putExtra("username", username);
                        i1.putExtra("error_message", error_message + "\n");
                        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i1);
                        finish();
                    } else {

                        File file = new File(
                                Environment.getExternalStorageDirectory(),
                                "mntv_live" + version + ".apk");

                        if (file.exists()) {
                            file.delete();
                        }
                        new AllowCountry().execute();

//                        String linkVersionCheck = LinkConfig.getString(
//                                EntryPoint.this, LinkConfig.VERSION_CHECK)
//                                + "?apk=mntv_live&version=" + version;
//                        JSONParserVersionCheck versionCheck = new JSONParserVersionCheck(
//                                EntryPoint.this);
//
//                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
//                            versionCheck.execute(linkVersionCheck);
//                        } else {
//                            versionCheck.executeOnExecutor(
//                                    AsyncTask.THREAD_POOL_EXECUTOR,
//                                    linkVersionCheck);
//                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    // CustomDialogManager.dataNotFetched(EntryPoint.this);
                }




            }

        }

    }


    private class AllowCountry extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            String link = LinkConfig.getString(EntryPoint.this, LinkConfig.ALLOW_COUNTRY);
            return new DownloadUtil(link, EntryPoint.this).downloadStringContent();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals(DownloadUtil.NotOnline) || result.equals(DownloadUtil.ServerUnrechable)) {
                //CustomDialogManager.ReUsedCustomDialogs.noInternet(EntryPoint.this);
            } else {
                try {
                    JSONObject jo = new JSONObject(result);
                    if (jo.getBoolean("allow")) {


                        String linkVersionCheck = LinkConfig.getString(
                                EntryPoint.this, LinkConfig.VERSION_CHECK)
                                + "?apk=mntv_live&version=" + version;
                        JSONParserVersionCheck versionCheck = new JSONParserVersionCheck(
                                EntryPoint.this);

                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                            versionCheck.execute(linkVersionCheck);
                        } else {
                            versionCheck.executeOnExecutor(
                                    AsyncTask.THREAD_POOL_EXECUTOR,
                                    linkVersionCheck);
                        }
                    } else {

                        Intent i = new Intent(EntryPoint.this, UnauthorizedAccess.class);
                        i.putExtra("error_code", jo.getString("error_code"));
                        i.putExtra("error_message", jo.getString("error_msg"));
                        i.putExtra("ipAddress", jo.getString("IP"));
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();

                    }
                } catch (JSONException je) {
                    Logger.printStackTrace(je);
                    CustomDialogManager.dataNotFetched(EntryPoint.this);
                }
            }
        }
    }

    private class checkMac extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            DownloadUtil dutil = new DownloadUtil(params[0], EntryPoint.this);
            String json = dutil.downloadStringContent();


            Log.d("TAG", json);
            return json;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
                final CustomDialogManager noInternet = new CustomDialogManager(EntryPoint.this, CustomDialogManager.ALERT);
                noInternet.build();
                noInternet.setTitle("No Internet");
                noInternet.setMessage("Please Check Your Internet Connection");
                noInternet.setPositiveButton(getString(R.string.btn_dismiss), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noInternet.dismiss();
                        finish();
                    }
                });
                noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        noInternet.dismiss();
                        startActivity(i);


                    }
                });
                noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(
                                android.provider.Settings.ACTION_SETTINGS);

                        noInternet.dismiss();
                        startActivity(intent);

                    }
                });
                noInternet.show();
                // server not rechable
            } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                Log.d(TAG, "server_error");
                Intent i = new Intent(EntryPoint.this, UnauthorizedAccess.class);
                i.putExtra("error_code", "");
                i.putExtra("error_message",
                        getString(R.string.server_unreachable_message));
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

            } else {
                try {
                    JSONObject jobject = new JSONObject(result);
                    String mac_exits = jobject.getString("mac_exists");
                    if (mac_exits.equals("yes")) {

                        String linkVersionCheck = LinkConfig.getString(
                                EntryPoint.this, LinkConfig.VERSION_CHECK)
                                + "?apk=mntv_live&version=" + version;

                        Log.d("Version",linkVersionCheck);

                        JSONParserVersionCheck versionCheck = new JSONParserVersionCheck(
                                EntryPoint.this);

                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                            versionCheck.execute(linkVersionCheck);
                        } else {
                            versionCheck.executeOnExecutor(
                                    AsyncTask.THREAD_POOL_EXECUTOR,
                                    linkVersionCheck);
                        }

                    } else {


                        String errorCode = jobject.getString("error_code");
                        String errorMessage = jobject.getString("error_message");
                        Intent i = new Intent(EntryPoint.this,
                                UnauthorizedAccess.class);
                        i.putExtra("error_code", "");
                        i.putExtra("error_message", errorMessage + "\n");
                        startActivity(i);
                    }


                } catch (JSONException e) {
                    Intent i = new Intent(EntryPoint.this,
                            UnauthorizedAccess.class);
                    i.putExtra("error_code", "");
                    i.putExtra("error_message", getString(R.string.unauthorized_access_message) + "\n");
                    startActivity(i);

                    e.printStackTrace();
                }
//            ApkVersionCheck versionCheck = new  ApkVersionCheck();
//            versionCheck.execute("http://hoteltv.newitventure.com/mybuilt/MarketAppInfo.php");

            }
        }
    }



    public class JSONParserVersionCheck extends
            AsyncTask<String, String, String> {

        Activity activity = null;

        public JSONParserVersionCheck(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected String doInBackground(String... params) {
            Log.d("EntryPoint: JsonParserVersionCheck", params[0]);
            DownloadUtil dUtil = new DownloadUtil(params[0], EntryPoint.this);

            return dUtil.downloadStringContent();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
                final CustomDialogManager noInternet = new CustomDialogManager(EntryPoint.this, CustomDialogManager.ALERT);
                noInternet.build();
                noInternet.setTitle("No Internet");
                noInternet.setMessage("Please Check Your Internet Connection");
                noInternet.setPositiveButton(getString(R.string.btn_dismiss), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noInternet.dismiss();
                        finish();
                    }
                });
                noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        noInternet.dismiss();
                        startActivity(i);


                    }
                });
                noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(
                                android.provider.Settings.ACTION_SETTINGS);

                        noInternet.dismiss();
                        startActivity(intent);

                    }
                });
                noInternet.show();
                // server not rechable
            } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                Log.d(TAG, "server_error");
                Intent i = new Intent(EntryPoint.this, UnauthorizedAccess.class);
                i.putExtra("error_code", "");
                i.putExtra("error_message",
                        getString(R.string.server_unreachable_message));
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

            } else {
                try {
                    JSONObject jObj = new JSONObject(result);
                    boolean updateRequired = jObj.getBoolean("updateRequired");

                    if (updateRequired) {
                        final String updateLink = jObj
                                .getString("updateLocation");

                        final CustomDialogManager update = new CustomDialogManager(EntryPoint.this, CustomDialogManager.ALERT);
                        update.build();
                        update.setTitle(getString(R.string.app_name));
                        update.setMessage(getString(R.string.update_message));
                        update.setPositiveButton("Update", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                APKDownloader installer = new APKDownloader(
                                        activity);
                                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                                    installer
                                            .execute(updateLink);
                                } else {
                                    installer
                                            .executeOnExecutor(
                                                    AsyncTask.THREAD_POOL_EXECUTOR,
                                                    updateLink);
                                }
                            }
                        });

                        update.setNegativeButton("Skip", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                update.dismiss();
                                Intent i1 = new Intent(EntryPoint.this,
                                        ChannelPlayActivity.class);
                                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i1.putExtra("version", version);
                                i1.putExtra("url", channel_url);
                                startActivity(i1);
                                finish();
                            }
                        });
                        update.show();
                    } else {
                        Intent i1 = new Intent(EntryPoint.this,
                                ChannelPlayActivity.class);
                        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i1.putExtra("version", version);
                        i1.putExtra("url", channel_url);
                        startActivity(i1);
                        finish();

							/*
							 * validCheck.executeOnExecutor(
							 * AsyncTask.THREAD_POOL_EXECUTOR,
							 * getString(R.string.base_url) +
							 * "check_json_test.php?boxId=" + macAddress);
							 */
                    }

                } catch (JSONException e) {
                    // Log.e("JSON Parser", "Error parsing data " +
                    // e.toString());
                }
            }
        }
    }



    public class APKDownloader extends AsyncTask<String, Integer, File> {
        ProgressDialog mProgressDialog;
        InputStream is = null;
        Activity activity = null;
        private PowerManager.WakeLock mWakeLock;

        public APKDownloader(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(EntryPoint.this);
            mProgressDialog.setMessage("Downloading the update");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(false);

            PowerManager pm = (PowerManager) activity.getApplicationContext()
                    .getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();

            mProgressDialog.show();
        }

        @Override
        protected File doInBackground(String... params) {
            // TODO Auto-generated method stub
            File file = null;

            try {
                // set the download URL, a url that points to a file on the
                // internet
                // this is the file to be downloaded
                String updateLink = params[0];

                URL url = new URL(updateLink);

                // create the new connection
                HttpURLConnection urlConnection = (HttpURLConnection) url
                        .openConnection();

                // set up some things on the connection
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);

                // and connect!
                urlConnection.connect();

                File SDCardRoot = Environment.getExternalStorageDirectory();
                String fileName = updateLink.substring(updateLink
                        .lastIndexOf("/"));

                file = new File(SDCardRoot, fileName);

                // this will be used to write the downloaded data into the file
                // we created
                FileOutputStream fileOutput = new FileOutputStream(file);

                // this will be used in reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                // this is the total size of the file
                int totalSize = urlConnection.getContentLength();
                // variable to store total downloaded bytes
                int downloadedSize = 0;

                // create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0; // used to store a temporary size of the
                // buffer

                // now, read through the input buffer and write the contents to
                // the file
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    // add the data in the buffer to the file in the file output
                    // stream (the file on the sd card
                    fileOutput.write(buffer, 0, bufferLength);
                    // add up the size so we know how much is downloaded
                    downloadedSize += bufferLength;
                    // Log.d( "com.globalhimalayan.smartvision.EntryPoint",
                    // downloadedSize + "" );

                    // this is where you would do something to report the
                    // prgress, like this maybe
                    // updateProgress(downloadedSize, totalSize);

                    if (totalSize > 0) {
                        publishProgress((downloadedSize * 100 / totalSize));
                    }

                }
                // close the output stream when done
                fileOutput.close();
            } catch (Exception e) {
                // Log.e("Buffer Error", "Error converting result " +
                // e.toString());

                AlertDialog dialog = new AlertDialog.Builder(activity)
                        .setPositiveButton("OK", null)
                        .setIcon(R.drawable.mntv_icon)
                        .setMessage("Error: " + e.toString()).create();

                dialog.show();
            }

            return file;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(File savedFile) {
            super.onPostExecute(savedFile);

            mWakeLock.release();
            mProgressDialog.dismiss();

            // Log.d("com.globalhimalayan.smartvision.EntryPoint",
            // "Saved File at: " + savedFile.toString());
            if (savedFile.exists()) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(savedFile),
                            "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    activity.getApplicationContext().startActivity(intent);
                    finish();

                } catch (Exception e) {
                    Logger.printStackTrace(e);
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

