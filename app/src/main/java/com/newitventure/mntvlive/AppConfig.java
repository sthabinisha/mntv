package com.newitventure.mntvlive;

/**
 * Created by binisha on 7/5/15.
 */
public class AppConfig {

    private static final boolean isInDevelopment=false;
    public static final int CACHE_HOLD_DURATION =60 * 1000; // 1 minutes
    public static final int randomdialogshowrange =2 * 60 * 60 * 1000;
    public static boolean isDevelopment() {
        return isInDevelopment;
    }

    public static String getMacAddress() {
//		return "acdbdaff02f5";
        return "00226d0c7781";
    }


    public static String getPrefsName() {
        return "HotelTV_PREFS";
    }
}
