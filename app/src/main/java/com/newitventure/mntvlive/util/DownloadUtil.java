package com.newitventure.mntvlive.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


import com.newitventure.mntvlive.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;


public class DownloadUtil {

    public static String NotOnline = "1";
    public static String ServerUnrechable = "0";
    private static final String TAG = "com.newitventure.mntvlive.util.DownloadUtil";

    private String link;
    private Context context;
    private String encoding = "utf-8";

    public DownloadUtil(String link, Context context) {
        this.link = link;
        this.context = context;
    }



    public String downloadStringContent() {
        if (isOnline()) {
//            if (isServerReachable(LinkConfig.CHECK_IF_SERVER_RECHABLE)) {

                String responseString = "";

                try {
                    DefaultHttpClient httpClient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(link);



                    HttpResponse httpResponse = httpClient.execute(httpGet);
                    HttpEntity httpEntity = httpResponse.getEntity();

                    responseString = EntityUtils.toString(httpEntity);

                    // Log.i(TAG, json + "------" );
                } catch (UnsupportedEncodingException e) {

                    Log.e(TAG, "UnsupportedEncodingException: " + e.toString());

                } catch (ClientProtocolException e) {

                    Log.e(TAG, "ClientProtocolException: " + e.toString());

                } catch (IOException e) {

                    Log.e(TAG, "IOException: " + e.toString());

                    Log.d(TAG, "server_error");
                    Intent i = new Intent(context, UnauthorizedAccess.class);
                    i.putExtra("error_code", "");
                    i.putExtra("error_message",
                            context.getString(R.string.server_unreachable_message));
                   // i.putExtra("username", EntryPoint.display_name);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                    ((Activity) context).finish();


                } catch (Exception e) {

                    Log.e(TAG, "Exception: " + e.toString());

                }

                return responseString;
//            } else
//                return ServerUnrechable;
        } else
            return NotOnline;

    }


//
    private boolean isServerReachable(String value)
    // To check if server is reachable
    {
        try {
            Log.d("checking if server rechable", "test test");
            InetAddress.getByName(value).isReachable(3000); // Replace
            // with
            // your
            // name
            // InetAddress.getByName("asdksjdjkshdd.askjdhasjk.sakd").isReachable(3000);
            // //Replace with your name
            Log.d(TAG, "connection established");
            return true;
        } catch (Exception e) {
            Log.d(TAG, "connection lost");
            return false;
        }
    }

    private boolean isOnline() {
        Log.d("checking if online", "test test");
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()== true) {
            return true;
        }else {
            return false;
        }
    }
}
