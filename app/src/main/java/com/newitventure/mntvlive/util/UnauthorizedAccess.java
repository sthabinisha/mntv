package com.newitventure.mntvlive.util;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.newitventure.mntvlive.AppConfig;
import com.newitventure.mntvlive.EntryPoint;
import com.newitventure.mntvlive.R;
import com.newitventure.mntvlive.entity.GetMac;
import com.newitventure.mntvlive.util.common.Logger;


public class UnauthorizedAccess extends Activity {

	private TextView messageView,txt_username, version;
	private Button retry;
	private String username,macAddress="",error_code="",error_message="", ipAddress = "";
	private StringBuilder mb = new StringBuilder();
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.unauthorized_access);
		messageView = (TextView) findViewById(R.id.unauthorized_message);
		txt_username = (TextView) findViewById(R.id.username);
		version= (TextView) findViewById(R.id.version);
		retry =(Button)findViewById(R.id.retry);
		retry.requestFocus();

		if (AppConfig.isDevelopment()) {
			macAddress = AppConfig.getMacAddress();
		} else {
			macAddress = GetMac.getMac(this); // Getting mac addresss
		}
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(
					getPackageName(), 0);
			String versionName = pInfo.versionName;

			version.setText(versionName);
		} catch (PackageManager.NameNotFoundException e) {
			Logger.printStackTrace(e);
		}

		error_code=getIntent().getStringExtra("error_code");
		error_message = getIntent().getStringExtra("error_message");
		Log.e(error_code+"", error_message+"");
		try{
			username = getIntent().getStringExtra("username");
			txt_username.setText(username);
			txt_username.setVisibility(View.VISIBLE);
			Log.e("username", username);
		}catch(Exception e){
			txt_username.setVisibility(View.GONE);
		}
		try {
			ipAddress = getIntent().getStringExtra("ipAddress");
			if(ipAddress!=null ){
				mb.append("Ip Adress:\t").append(ipAddress).append("\n\n");
			}
		} catch (Exception e) {
			Logger.printStackTrace(e);
			ipAddress = "";
		}
		if(error_code.equals(""))
			mb.append("Box ID: "+ macAddress. toUpperCase())
					.append( "\n\n" )
					.append( error_message );

		else
			mb.append("Box ID: "+ macAddress. toUpperCase())
					.append( "\n" )
					.append("Error Code: "+ error_code)
					.append("\n")
					.append( error_message );

		messageView.setText( mb.toString() );
		retry.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(UnauthorizedAccess.this,EntryPoint.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
			}
		});
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
}
