package com.newitventure.mntvlive.util.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newitventure.mntvlive.AppConfig;
import com.newitventure.mntvlive.ChannelPlayActivity;
import com.newitventure.mntvlive.R;
import com.newitventure.mntvlive.entity.GetMac;


public class CustomDialogManager {
	public static final int DEFAULT = 0;
	public static final int LOADING = 1;
	public static final int PROGRESS = 2;
	public static final int ALERT = 3;
	public static final int MESSAGE = 4;

	private Context context = null;
	private String version = "", macAddress = "", title = "", message = "";

	private Dialog d;
	private TextView alertTitle, MacTextView, versionTextView, messageTextView;
	private RelativeLayout macAndVersion;
	private LinearLayout buttonLayout;
	private Button positive, neutral, negative, extraBtn;
	private ImageView error_image;
	private ProgressBar progressBar;
	private View viewBelowMac, viewAboveButtons;

	private int type = DEFAULT;

	public CustomDialogManager(Context context, int type) {
		this.context = context;
		this.type = type;
		//this.title = context.getString(R.string.app_name);
		this.message = context.getString(R.string.unexpected_error);
		d = new Dialog(context);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.custom_dialog);
		d.setCancelable(false);

	}

	public CustomDialogManager(Context context, String title, int type) {
		this.context = context;
		this.type = type;
		
		this.title = title;
		d = new Dialog(context);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.custom_dialog);
		d.setCancelable(false);
	}

	public CustomDialogManager(Context context, String title, String message,
							   int type) {
		this.context = context;
		this.type = type;
		this.title = title;
		this.message = message;
		d = new Dialog(context);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.custom_dialog);
		d.setCancelable(false);
	}

	public void build() {


		alertTitle = (TextView) d.findViewById(R.id.dialog_heading);
		alertTitle.setText(title);
		macAndVersion = (RelativeLayout) d.findViewById(R.id.mac_version);
		MacTextView = (TextView) d.findViewById(R.id.macaddress_variable);
		versionTextView = (TextView) d.findViewById(R.id.app_version_variable);

		viewBelowMac = (View) d.findViewById(R.id.view_below_mac);
		viewAboveButtons = (View) d.findViewById(R.id.view_above_button);

		progressBar = (ProgressBar) d.findViewById(R.id.progressBar);
		error_image = (ImageView) d.findViewById(R.id.error_image);
		messageTextView = (TextView) d.findViewById(R.id.message);

		buttonLayout = (LinearLayout) d.findViewById(R.id.button_layout);
		positive = (Button) d.findViewById(R.id.positive);
		neutral = (Button) d.findViewById(R.id.neutral);
		negative = (Button) d.findViewById(R.id.negative);
		extraBtn = (Button) d.findViewById(R.id.extraBtn);

		macAndVersion.setVisibility(View.GONE);
		progressBar.setVisibility(View.GONE);
		error_image.setVisibility(View.GONE);
		viewAboveButtons.setVisibility(View.GONE);
		viewBelowMac.setVisibility(View.GONE);
		buttonLayout.setVisibility(View.GONE);
		positive.setVisibility(View.GONE);
		neutral.setVisibility(View.GONE);
		negative.setVisibility(View.GONE);
		extraBtn.setVisibility(View.GONE);
		neutral.requestFocus();
		positive.clearFocus();

		if (type == LOADING) {
			alertTitle.setVisibility(View.GONE);
			error_image.setVisibility(View.GONE);
			progressBar.setVisibility(View.VISIBLE);
			viewBelowMac.setVisibility(View.GONE);
			this.message = context.getString(R.string.loading);
			messageTextView.setText(this.message);
		} else if (type == PROGRESS) {
			alertTitle.setVisibility(View.VISIBLE);
			macAndVersion.setVisibility(View.GONE);
			viewAboveButtons.setVisibility(View.GONE);
			error_image.setVisibility(View.GONE);
			progressBar.setVisibility(View.VISIBLE);
			messageTextView.setText(this.message);
		} else if (type == ALERT) {
			error_image.setVisibility(View.VISIBLE);
			progressBar.setVisibility(View.GONE);
			messageTextView.setText(this.message);
		} else if (type == MESSAGE) {
			alertTitle.setVisibility(View.VISIBLE);
			macAndVersion.setVisibility(View.GONE);
			error_image.setVisibility(View.GONE);
			progressBar.setVisibility(View.GONE);
			messageTextView.setText(this.message);
		}

	}

	public Button setPositiveButton(String btn_text,
			View.OnClickListener onClickListener) {
		positive = (Button) d.findViewById(R.id.positive);
		positive.requestFocus();
		viewAboveButtons.setVisibility(View.VISIBLE);
		buttonLayout.setVisibility(View.VISIBLE);
		positive.setText(btn_text);
		positive.setVisibility(View.VISIBLE);
		positive.setOnClickListener(onClickListener);
		return positive;

	}

	public Button setNeutralButton(String btn_text,
			View.OnClickListener onClickListener) {
		neutral = (Button) d.findViewById(R.id.neutral);
		neutral.requestFocus();
		viewAboveButtons.setVisibility(View.VISIBLE);
		buttonLayout.setVisibility(View.VISIBLE);
		neutral.setText(btn_text);
		neutral.setVisibility(View.VISIBLE);
		neutral.setOnClickListener(onClickListener);
		return neutral;

	}

	public Button setNegativeButton(String btn_text,
			View.OnClickListener onClickListener) {
		negative = (Button) d.findViewById(R.id.negative);
		negative.requestFocus();
		viewAboveButtons.setVisibility(View.VISIBLE);
		buttonLayout.setVisibility(View.VISIBLE);
		negative.setText(btn_text);
		negative.setVisibility(View.VISIBLE);
		negative.setOnClickListener(onClickListener);
		return negative;

	}

	public Button setExtraButton(String btn_text,
			View.OnClickListener onClickListener) {
		extraBtn = (Button) d.findViewById(R.id.extraBtn);
		extraBtn.requestFocus();
		viewAboveButtons.setVisibility(View.VISIBLE);
		buttonLayout.setVisibility(View.VISIBLE);
		extraBtn.setText(btn_text);
		extraBtn.setVisibility(View.VISIBLE);
		extraBtn.setOnClickListener(onClickListener);
		return extraBtn;

	}

	public void setTitle(String title) {
		this.title = title;
		alertTitle = (TextView) d.findViewById(R.id.dialog_heading);
		alertTitle.setText(title);
		alertTitle.setVisibility(View.VISIBLE);
	}

	public void setMessage(String message) {
		this.message = message;
		messageTextView = (TextView) d.findViewById(R.id.message);
		messageTextView.setText(message);
	}

	public void showMacAndVersion() {
		if (AppConfig.isDevelopment()) {
			macAddress = AppConfig.getMacAddress();
		} else {
			macAddress = GetMac.getMac(context);
		}
		try {
			PackageInfo pInfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			version = pInfo.versionName;
		} catch (NameNotFoundException e) {
			version = "N/A";
			Logger.printStackTrace(e);
		}
		MacTextView.setText(macAddress);
		versionTextView.setText(version);

		macAndVersion.setVisibility(View.VISIBLE);
		viewBelowMac.setVisibility(View.VISIBLE);
	}

	public void show() {
		try {
			d.show();
		}catch(Exception e){}
	}

	public void hide() {
		try {
			d.hide();
		}catch(Exception e){}
	}

	public Dialog getInnerObject(){
		return d;
	}
	public void dismiss() {
		try {
			d.dismiss();
		}catch(Exception e){}

	}

	public boolean isShowing() {
		try {
			return d.isShowing();
		}catch(Exception e){
			return false;

		}


	}
	public Button getNeutralButton(){
		return neutral;
	}

	public static void dataNotFetched(final Context context){
	    final CustomDialogManager error = new CustomDialogManager(context, CustomDialogManager.ALERT);
	    error.build();
	    error.showMacAndVersion();
	    error.setMessage(context.getString(R.string.json_exception));
	    error.setNegativeButton("Dismiss", new View.OnClickListener() {
	     
	     @Override
	     public void onClick(View v) {
	      error.dismiss();
	      ((Activity)context).finish();
	      
	     }
	    });
	    error.show();
	   }

	public static CustomDialogManager epg(final Context context){
		final CustomDialogManager error = new CustomDialogManager(context, CustomDialogManager.DEFAULT);
		error.build();
		error.setMessage(context.getString(R.string.json_exception));
		error.setNegativeButton("Dismiss", new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				error.dismiss();
				((Activity)context).finish();

			}
		});
		error.show();
		return error;
	}
	public  static CustomDialogManager noInternetDialog(final Context context){
		final CustomDialogManager noInternet = new CustomDialogManager(context, CustomDialogManager.ALERT);
		noInternet.build();
		noInternet.setTitle(context.getString(R.string.app_name));
		noInternet.setMessage(context.getString(R.string.server_unreachable_message));


		noInternet.setPositiveButton(context.getString(R.string.action_settings), new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intentt = new Intent(
						android.provider.Settings.ACTION_SETTINGS);
				noInternet.dismiss();

				context.startActivity(intentt);


			}
		});
		noInternet.show();

		return noInternet;
	}

	public  static CustomDialogManager channelDowm(final Context context, final String channel_url, final String version){
		final CustomDialogManager noInternet = new CustomDialogManager(context, CustomDialogManager.ALERT);
		noInternet.build();
		noInternet.setTitle(context.getString(R.string.app_name));
		noInternet.setMessage(context.getString(R.string.server_unreachable_message));


		noInternet.setNegativeButton(context.getString(R.string.reconnect), new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i1 = new Intent(context,
						ChannelPlayActivity.class);
				i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i1.putExtra("version", version);
				i1.putExtra("url", channel_url);
				((Activity)context).startActivity(i1);
				((Activity)context).finish();



			}
		});
		noInternet.setPositiveButton(context.getString(R.string.btn_dismiss), new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				((Activity)context).finish();

			}
		});
		noInternet.show();

		return noInternet;
	}


	public void setRelaunchButton(final Context context) {
		setNeutralButton("RE-Launch", new View.OnClickListener() {
			@Override
			public void onClick(View view) {
					Intent i3 = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
					if (i3 != null) {
						//throw new PackageManager.NameNotFoundException();
						i3.addCategory(Intent.CATEGORY_LAUNCHER);
						context.startActivity(i3);
					}
			}
		});
	}
}
