package com.newitventure.mntvlive.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.newitventure.mntvlive.R;
import com.newitventure.mntvlive.util.common.Logger;


public class NetworkUtil {

    public static final int NETWORK_STATE_UNKNOWN = 1000;
    public static final int NETWORK_NOT_AVAILABLE = 1001;
    public static final int SERVER_HOST_NOT_AVAILABLE = 1002;

    public static final int NETWORK_STATE_UNKNOWN_MESSAGE = R.string.internet_not_available;
    public static final int NETWORK_NOT_AVAILABLE_MESSAGE = R.string.internet_not_available;
    public static final int SERVER_HOST_NOT_AVAILABLE_MESSAGE = R.string.server_not_available;

    private static final String TAG = "NetworkUtil";
    public static boolean isConnected3G = false;
    public static boolean isConnectedWifi = false;
    public static boolean isConnectedLan = false;
    public static NetworkInfo netInfo;

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static final int NETWORK_STATUS_NOT_CONNECTED=0,NETWORK_STAUS_WIFI=1,NETWORK_STATUS_MOBILE=2;


    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        netInfo = cm.getActiveNetworkInfo();

        try {

            //Toast.makeText(context, netInfo.getExtraInfo()+String.valueOf(netInfo), Toast.LENGTH_SHORT).show();

//            Toast.makeText(context, netInfo.toString(), Toast.LENGTH_LONG).show();

            String networkInfo = cm.getActiveNetworkInfo().getExtraInfo().toUpperCase().replace("\"", "");
            System.out.println(networkInfo);


            if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null) {
                isConnected3G = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
//                Toast.makeText(context, "I am mobile", Toast.LENGTH_SHORT).show();
            }


            if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null) {
                isConnectedWifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
                Log.v("the wifi coonnection is", String.valueOf(isConnectedWifi));
//                Toast.makeText(context, "I am wifi", Toast.LENGTH_SHORT).show();


            }


            if (cm.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET) != null || cm.getActiveNetworkInfo().getExtraInfo().equals(null)) {

                isConnectedLan = cm.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET).isConnected();
                Log.v("the lan coonnection is", String.valueOf(isConnectedLan));
//                Toast.makeText(context, "I am lan2", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Logger.printStackTrace(e);
            if (netInfo == null) {
                return false;
            }
//            Toast.makeText(context, "I am catch", Toast.LENGTH_SHORT).show();


            isConnectedLan = cm.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET).isConnected();

            if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null) {
                isConnectedWifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
                Log.v("the wifi coonnection is", String.valueOf(isConnectedWifi));
//                Toast.makeText(context, "I am wifi", Toast.LENGTH_SHORT).show();


            }

        }


        //if (netInfo != null && netInfo.isConnectedOrConnecting()) {
        if (netInfo != null && (isConnected3G || isConnectedWifi || isConnectedLan)) {
//                Toast.makeText(context, "I am true", Toast.LENGTH_SHORT).show();
            return true;
        }
//        Toast.makeText(context, "I am false", Toast.LENGTH_SHORT).show();
        return false;

    }
    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {

                return TYPE_WIFI;


        }
        return TYPE_NOT_CONNECTED;
    }

    public static int getConnectivityStatusString(Context context) {
        int conn = NetworkUtil.getConnectivityStatus(context);
        int status = 0;
        if (conn == NetworkUtil.TYPE_WIFI) {
            status = NETWORK_STAUS_WIFI;
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            status = NETWORK_STATUS_MOBILE;
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            status = NETWORK_STATUS_NOT_CONNECTED;
        }
        return status;
    }


    // To check if server is reachable
    public static boolean isServerReachable() {
        // this method cannot be run on main thread
        // currently looking for alternative solution to detect if server is reachable
        /*
		try {
			InetAddress.getByName("http://www.nepalchannels.com").isReachable(50000); // Replace

			Logger.d(TAG, "connection established");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.d(TAG, "connection lost");
			return false;
		}*/

        return true;
    }

    public static boolean isInternetAvailable(Context context) {


        if (isOnline(context)) {
            return true;
        } else {


            int cause = NetworkUtil.NETWORK_STATE_UNKNOWN;
            int message = NetworkUtil.NETWORK_STATE_UNKNOWN_MESSAGE;

            if (!isOnline(context)) {
                cause = NetworkUtil.NETWORK_NOT_AVAILABLE;
                message = NetworkUtil.NETWORK_NOT_AVAILABLE_MESSAGE;
            } else {
                cause = NetworkUtil.SERVER_HOST_NOT_AVAILABLE;
                message = NetworkUtil.SERVER_HOST_NOT_AVAILABLE_MESSAGE;
            }




            return false;
        }
    }
    public static boolean isInternetAvailableNext(Context context) {

        if (isOnline(context)) {
            return true;
        } else {

//            Intent intent = new Intent(context, NoInternet.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            context.startActivity(intent);
//            finish();
//            int cause = NetworkUtil.NETWORK_STATE_UNKNOWN;
//            int message = NetworkUtil.NETWORK_STATE_UNKNOWN_MESSAGE;
//
//            if (!isOnline(context)) {
//                cause = NetworkUtil.NETWORK_NOT_AVAILABLE;
//                message = NetworkUtil.NETWORK_NOT_AVAILABLE_MESSAGE;
//            } else {
//                cause = NetworkUtil.SERVER_HOST_NOT_AVAILABLE;
//                message = NetworkUtil.SERVER_HOST_NOT_AVAILABLE_MESSAGE;
//            }




            return false;
        }
    }
}
