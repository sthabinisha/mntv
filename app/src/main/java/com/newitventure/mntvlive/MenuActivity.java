package com.newitventure.mntvlive;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import com.newitventure.mntvlive.entity.LinkConfig;
import com.newitventure.mntvlive.util.DownloadUtil;
import com.newitventure.mntvlive.util.UnauthorizedAccess;
import com.newitventure.mntvlive.util.common.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MenuActivity extends Activity {
    ListView listView;
    String[] values;
    String mac_link, channel_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_menu);
        WindowManager.LayoutParams wmlp = getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;// | Gravity.LEFT;

        getWindow().setLayout(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);

        listView= (ListView) findViewById(R.id.listView);
        listView.setVisibility(View.VISIBLE);
        final String macAddress = getIntent().getExtras().getString("macAddress");


        values = new String[]{"My Account","Menu","Applications", "Reconnect", "Re-Launch", "Settings"};
        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }
        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);



                switch ( itemPosition ) {
                    case 0:
                      /*  Intent i = new Intent(
                                MenuActivity.this, AccountActivity.class);

                        startActivity(i);*/

                        break;
                    case 1:


                        Intent i2 = new Intent(Intent.ACTION_MAIN);
                        i2.addCategory(Intent.CATEGORY_HOME);
                        finish();
                        i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i2);
                        break;
                    case 2:
                        Intent intApp = new Intent(
                                MenuActivity.this, AppList.class);
                        finish();

                        startActivity(intApp);

                        break;
                    case 3:

                        mac_link = LinkConfig.getString(getApplicationContext(),
                                LinkConfig.CHANNEL_URL);
                        new channeljson().execute(mac_link);
                        break;
                    case 4:

                        Intent i3 = new Intent(
                                MenuActivity.this, EntryPoint.class);
                        i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);


                        startActivity(i3);
                        finish();

                        break;
                    case 5:
                        Intent intentt = new Intent(
                                android.provider.Settings.ACTION_SETTINGS);

                        startActivity(intentt);

                        break;


                    default:
                        break;
                }

            }

        });
    }






    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class channeljson extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            DownloadUtil dutil = new DownloadUtil(params[0], MenuActivity.this);
            String json = dutil.downloadStringContent();


            Log.d("TAG", json);
            return json;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


                try {
                    JSONObject jobject = new JSONObject(result);
                    if (jobject.has("link")) {


                        channel_url = jobject.getString("link");

                        Intent i1 = new Intent(MenuActivity.this,
                                ChannelPlayActivity.class);
                        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i1.putExtra("url", channel_url);
                        startActivity(i1);
                        finish();
                    } else {


                        String errorCode = jobject.getString("error_code");
                        String errorMessage = jobject.getString("error_message");
                        Intent i = new Intent(MenuActivity.this,
                                UnauthorizedAccess.class);
                        i.putExtra("error_code", "");
                        i.putExtra("error_message", errorMessage + "\n");
                        startActivity(i);
                    }


                } catch (JSONException e) {
                    Intent i = new Intent(MenuActivity.this,
                            UnauthorizedAccess.class);
                    i.putExtra("error_code", "");
                    i.putExtra("error_message", getString(R.string.unauthorized_access_message) + "\n");
                    startActivity(i);

                    Logger.printStackTrace(e);
                }
//            ApkVersionCheck versionCheck = new  ApkVersionCheck();
//            versionCheck.execute("http://hoteltv.newitventure.com/mybuilt/MarketAppInfo.php");


        }
    }
}
