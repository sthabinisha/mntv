package com.newitventure.mntvlive.entity;

import android.content.Context;

import com.newitventure.mntvlive.R;

/**
 * Created by Niroj on 18/03/2016.
 */
public class LinkConfig {

    public static final String BASE_URL = "http://mntvbox.com/json/";
    public static final int MAC_EXISTS = R.string.check_mac;
    public static final int VERSION_CHECK = R.string.version_check_url;
    public static final int CHANNEL_URL = R.string.channel_link;
    public static final int ABOUT_US = R.string.about_us;
    public static final int ALLOW_COUNTRY=R.string.allow_country;



    public static String getString(Context context, int resId) {
        return BASE_URL + context.getResources().getString(resId);
    }

}
