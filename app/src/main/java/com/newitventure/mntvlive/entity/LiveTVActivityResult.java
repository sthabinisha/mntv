package com.newitventure.mntvlive.entity;

/**
 * Created by binisha on 4/15/16.
 */
public class LiveTVActivityResult {
    public static final int REQUEST_NULL = 0;
    public static final int REQUEST_PLAY = 1;

    public static final int RESULT_NULL = 0;
    public static final int RESULT_DVR = 1;
    public static final int RESULT_CHANNEL = 2;


}
