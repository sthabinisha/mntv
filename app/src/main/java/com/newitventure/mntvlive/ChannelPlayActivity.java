package com.newitventure.mntvlive;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newitventure.mntvlive.entity.GetMac;
import com.newitventure.mntvlive.entity.LinkConfig;
import com.newitventure.mntvlive.entity.Mcrypt;
import com.newitventure.mntvlive.util.DownloadUtil;
import com.newitventure.mntvlive.util.NetworkUtil;
import com.newitventure.mntvlive.util.UnauthorizedAccess;
import com.newitventure.mntvlive.util.VideoControllerView;
import com.newitventure.mntvlive.util.common.CustomDialogManager;
import com.newitventure.mntvlive.util.common.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class ChannelPlayActivity extends Activity implements
        SurfaceHolder.Callback, MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener,
        VideoControllerView.MediaPlayerControl, MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener{
    MediaPlayer player;
    MediaController mController;
    //SurfaceView videoSurface;
    String macAddress, data1=null, deryptedUrl;
    //    VideoView videoview;
    String data="", version;
    String channel_url, channel_name;
    VideoControllerView controller;
    SurfaceView videoSurface;
    ListView listView;
    private Runnable runnableToShowMac, runnableToHideMac;
    private CustomDialogManager noInternet=null,channelDown=null;
    private TextView channelHeadingOption, titleChannel ;
    Timer TimerMacCheck;
    RelativeLayout relativeLayout;
    private ProgressBar progressBar;
    String aboutUs, description;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_play);





        if (AppConfig.isDevelopment()) {
            macAddress = AppConfig.getMacAddress();
        } else {
            macAddress = GetMac.getMac(ChannelPlayActivity.this);
        }
        videoSurface = (SurfaceView) findViewById(R.id.videoSurface);
        listView= (ListView) findViewById(R.id.listView);
        channelHeadingOption = (TextView)findViewById(R.id.channelHeadingOption);
        relativeLayout = (RelativeLayout) findViewById(R.id.menulayout);
        titleChannel = (TextView) findViewById(R.id.movie_title);
        titleChannel.setText("MNTV Live");
        version= getIntent().getStringExtra("version");


        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBar.bringToFront();
        try {
            SurfaceHolder videoHolder = videoSurface.getHolder();
            videoHolder.addCallback(this);
            player = new MediaPlayer();
            controller = new VideoControllerView(this,false);
        } catch (Exception e) {
            Logger.printStackTrace(e);
        }


        checkInternetConnection();

//
//        NetworkChangeReceiver na = new NetworkChangeReceiver( this );
//        registerReceiver( na, new IntentFilter("com.nitv.wodlivechannels.sessionService") );




        controller.setMediaPlayer(this);
        controller
                .setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));

    }


//    public static class NetworkChangeReceiver extends BroadcastReceiver {
//
//        ChannelPlayActivity mActivity;
//
//        public NetworkChangeReceiver() {}
//
//        public NetworkChangeReceiver( ChannelPlayActivity activity) {
//
//            mActivity = activity;
//        }
//
//        @Override
//        public void onReceive(final Context context, final Intent intent) {
//
//
//            Log.d( "AA", "Context is " + context.getClass().getSimpleName() );
//            int status = NetworkUtil.getConnectivityStatusString(context);
//            if (!"android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
//                if (status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
//
//                    try {
//                        if (mActivity.channelDown.isShowing()) {
//                            mActivity.channelDown.dismiss();
//                        }
//                    } catch (Exception e1) {
//
//                    }
//
//                    try {
//                        if (!mActivity.noInternet.isShowing()) {
//                            mActivity.noInternet = CustomDialogManager.noInternetDialog(context);
//                            mActivity.noInternet.setMessage( mActivity.getString(R.string.server_unreachable_message));
//                            mActivity.noInternet.getInnerObject().setCancelable(true);
//                        }
//                    } catch (Exception e) {
//                        Log.d( "AA", "mActivity check");
//                        if( mActivity == null )
//                            Log.d( "AA", "mActivity is null");
//
//                        if( context == null )
//                            Log.d( "AA", "context is null");
//
//                        mActivity.noInternet = CustomDialogManager.noInternetDialog(context);
//                        mActivity.noInternet.setMessage(mActivity.getString(R.string.server_unreachable_message));
//                        mActivity.noInternet.getInnerObject().setCancelable(true);
//                    }
//                } else {
//                    try {
//                        if (mActivity.noInternet.isShowing()) {
//                            mActivity.noInternet.dismiss();
//                            if (!mActivity.player.isPlaying()) {
//                                mActivity.reconnect();
//                            } else {
//                                mActivity.reconnect();
//                            }
//                        }
//                    } catch (Exception e1) {
//
//                    }
////                    new ResumeForceExitPause(context).execute();
//                }
//
//            }
//        }
//    }

    private void checkInternetConnection() {
        Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {


                        if (!NetworkUtil.isInternetAvailable(ChannelPlayActivity.this)) {
                            try{
                                if(channelDown.isShowing()){
                                    channelDown.dismiss();
                                }
                            }catch (Exception e1){

                            }
                            try{
                                if(!noInternet.isShowing()){
                                    noInternet = CustomDialogManager.noInternetDialog(ChannelPlayActivity.this);
                                    noInternet.setMessage(getString(R.string.server_unreachable_message));
                                    noInternet.getInnerObject().setCancelable(true);
                                }
                            }catch (Exception e){
                                noInternet = CustomDialogManager.noInternetDialog(ChannelPlayActivity.this);
                                noInternet.setMessage(getString(R.string.server_unreachable_message));
                                noInternet.getInnerObject().setCancelable(true);
                            }


//


                        }else {
                            try{
                                if(noInternet.isShowing()){
                                    noInternet.dismiss();
//                                    Toast.makeText(ChannelPlayActivity.this,"oncreate",Toast.LENGTH_LONG).show();
                                    Intent i= new Intent(ChannelPlayActivity.this,ChannelPlayActivity.class);
                                    startActivity(i);
//                                    if(!player.isPlaying()){
//                                        Toast.makeText(ChannelPlayActivity.this,"if",Toast.LENGTH_LONG).show();
//                                        reconnect();
//                                    }else
//                                    {
//                                        Toast.makeText(ChannelPlayActivity.this,"else",Toast.LENGTH_LONG).show();
//                                        reconnect();
//                                    }
                                }
                            }catch (Exception e1){
//                                onCreate(new Bundle());

                            }
                        }

//



                    }

                });
            }

        }, 0, 5000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("hi");
//        player.reset();
//        try{
//            if(channelDown.isShowing())
//                channelDown.dismiss();
//        }catch (Exception e){}
//        try{
//            if(noInternet.isShowing())
//                noInternet.dismiss();
//        }catch (Exception e){}
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
//        Toast.makeText(ChannelPlayActivity.this,"eee",Toast.LENGTH_LONG).show();
        reconnect();



    }

    private void playNewVideo(String channel_url) {

        Mcrypt mCrypt = new Mcrypt();
        try {
            deryptedUrl = new String(mCrypt.decrypt(channel_url));
            channel_url = deryptedUrl.trim();
            Log.d("TAG" + "channel url after decryption", channel_url + "");
//            Toast.makeText(ChannelPlayActivity.this,channel_url,Toast.LENGTH_LONG).show();
        } catch (Exception e1) {
            Logger.printStackTrace(e1);
//            Toast.makeText(ChannelPlayActivity.this,"catch",Toast.LENGTH_LONG).show();
        }

        try{
            player.reset();
        }catch (Exception e){
            Logger.printStackTrace(e);
//            Toast.makeText(ChannelPlayActivity.this,"www",Toast.LENGTH_LONG).show();
        }


        try {


            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(this, Uri.parse(channel_url));
            player.prepare();
            player.setOnPreparedListener(this);
            player.setOnBufferingUpdateListener(this);
            player.setOnCompletionListener(this);
            player.setOnErrorListener(this);
//            Toast.makeText(ChannelPlayActivity.this,channel_url,Toast.LENGTH_LONG).show();
            // player.setOnInfoListener(this);
        } catch (IllegalArgumentException e) {
//            Toast.makeText(ChannelPlayActivity.this,"IllegalArgumentException",Toast.LENGTH_SHORT).show();

            reconnect();
            Logger.printStackTrace(e);
        } catch (SecurityException e) {
//            Toast.makeText(ChannelPlayActivity.this,"SecurityException",Toast.LENGTH_SHORT).show();
            reconnect();
            Logger.printStackTrace(e);
        } catch (IllegalStateException e) {
//            Toast.makeText(ChannelPlayActivity.this,"IllegalStateException",Toast.LENGTH_SHORT).show();
            reconnect();
            Logger.printStackTrace(e);
        }catch (IOException e) {
//            Toast.makeText(ChannelPlayActivity.this,"IOException",Toast.LENGTH_SHORT).show();
            try{
                if(noInternet.isShowing())
                    noInternet.dismiss();
            }catch (Exception e21){}
            try{

                if(!channelDown.isShowing()){
                    channelDown = CustomDialogManager.channelDowm(ChannelPlayActivity.this,channel_url, version);
                    channelDown.setMessage("Channel down,\nPlease Wait or Contact your service provider!");
                    channelDown.getInnerObject().setCancelable(true);
//                    final CustomDialogManager channelDown = new CustomDialogManager(ChannelPlayActivity.this, CustomDialogManager.ALERT);
//                    channelDown.build();
//                    channelDown.setTitle(getString(R.string.app_name));
//                    channelDown.setMessage("Channel down,\nPlease Wait or Contact your service provider!");
//
//
//                    channelDown.setNegativeButton(getString(R.string.reconnect), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            reconnect();
//
//
//
//                        }
//                    });
//                    channelDown.setPositiveButton(getString(R.string.btn_dismiss), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            finish();
//
//                        }
//                    });
//                    channelDown.show();
                }

            }catch (Exception e2){
                channelDown = CustomDialogManager.channelDowm(ChannelPlayActivity.this, channel_url, version);
                channelDown.setMessage("Channel down,\nPlease Wait or Contact your service provider!");
                channelDown.getInnerObject().setCancelable(true);
//                final CustomDialogManager channelDown = new CustomDialogManager(ChannelPlayActivity.this, CustomDialogManager.ALERT);
//                channelDown.build();
//                channelDown.setTitle(getString(R.string.app_name));
//                channelDown.setMessage("Channel down,\nPlease Wait or Contact your service provider!");
//
//
//                channelDown.setNegativeButton(getString(R.string.reconnect), new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        reconnect();
//
//
//
//                    }
//                });
//                channelDown.setPositiveButton(getString(R.string.btn_dismiss), new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//
//                    }
//                });
//                channelDown.show();
            }

            /*channelDown.setNegativeButton("Re-Connect", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        reconnect();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });*/
            reconnect();
            Logger.printStackTrace(e);
        }catch (Exception e){
            Logger.printStackTrace(e);
            reconnect();
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        controller.show();

      title();

        return false;
    }
    public  void  title(){
        titleChannel.setVisibility(View.VISIBLE);
        Timer t = new Timer(false);
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        titleChannel.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }, 5000);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        player.setDisplay(surfaceHolder);
//        player.prepareAsync();

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void start() {
        player.start();

    }

    @Override
    public void pause() {
//        Toast.makeText(ChannelPlayActivity.this,"pause",Toast.LENGTH_LONG).show();

    }

    @Override
    public int getDuration() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return 0;
    }

    @Override
    public void seekTo(int pos) {
        player.seekTo(pos);

    }


    @Override
    public boolean isPlaying() {
        return player.isPlaying();

    }

    @Override
    public int getBufferPercentage() {
//        Toast.makeText(this, "OnBufferingupdate:isbuffer perc update " , Toast.LENGTH_SHORT).show();
        return 0;
    }

    @Override
    public boolean canPause() {

        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
//            Toast.makeText(ChannelPlayActivity.this,"destroy",Toast.LENGTH_LONG).show();
            player.reset();
        }catch (Exception e){
            Logger.printStackTrace(e);
        }

    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public boolean isFullScreen() {
        return false;
    }

    @Override
    public void toggleFullScreen() {

    }

    @Override
    public String videoName() {
        return channel_name;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
//        Toast.makeText(ChannelPlayActivity.this,"ONCOmplete",Toast.LENGTH_LONG).show();
        try{player.reset();}catch (Exception e){}
        progressBar.setVisibility(View.VISIBLE);

        reconnect();

    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
//        Toast.makeText(ChannelPlayActivity.this,"onErrortyuiop",Toast.LENGTH_LONG).show();


        try{
            int status = NetworkUtil.getConnectivityStatusString(ChannelPlayActivity.this);
//            if (!"android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            if(status==NetworkUtil.NETWORK_STATUS_NOT_CONNECTED){
                if(!noInternet.isShowing()) {
                    noInternet = CustomDialogManager.noInternetDialog(ChannelPlayActivity.this);
                    noInternet.setMessage(getString(R.string.server_unreachable_message));
                    noInternet.getInnerObject().setCancelable(true);
                }else{
                    checkInternetConnection();
                }
            }else {

                reconnect();
            }
        }catch (Exception e){
//            Toast.makeText(ChannelPlayActivity.this,"catch error",Toast.LENGTH_LONG).show();
        }
        progressBar.setVisibility(View.VISIBLE);

//        reconnect();

        return true;
    }

    @Override
    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
//        Toast.makeText(ChannelPlayActivity.this,"oertyui",Toast.LENGTH_LONG).show();
        title();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        progressBar.setVisibility(View.INVISIBLE);
//        Toast.makeText(ChannelPlayActivity.this,"on prep",Toast.LENGTH_LONG).show();
//        connectingDialog.hide();
        try {
            if (channelDown.isShowing())
                channelDown.dismiss();
        }catch (Exception e){}

        try {
            if (noInternet.isShowing())
                noInternet.dismiss();
        }catch (Exception e){}

//        randomDisplayMacAddress();
//                player.start();
//

        videoSurface.postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                Intent service = new Intent();
                service.setComponent(new ComponentName("com.mytv.MyTVHome",
                        "com.mytv.utils.FullScreenVideoService"));
                Log.e("TAG" + ": on prepared", "call full screen");
                service.putExtra("fullscreen", true);
                startService(service);

            }
        }, 1000);

        player.start();

    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
//        Toast.makeText(ChannelPlayActivity.this,"update",Toast.LENGTH_LONG).show();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void randomDisplayMacAddress() {

        final Random random = new Random();

        final Handler handlerToShowMac = new Handler();
        final Handler handlerToHideMac = new Handler();

        final TextView tvboxID = (TextView) findViewById(R.id.tvboxID);

        final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                tvboxID.getLayoutParams());

        runnableToHideMac = new Runnable() {

            @Override
            public void run() {
                params.setMargins(500, random.nextInt(500),
                        random.nextInt(200), random.nextInt(200));
                tvboxID.setLayoutParams(params);
                tvboxID.setVisibility(View.GONE);
                System.out.println("box is invisible");

                handlerToShowMac.postDelayed(runnableToShowMac, 5 * 1000 * 60);
            }
        };

        runnableToShowMac = new Runnable() {

            @Override
            public void run() {
                params.setMargins(500, random.nextInt(500),
                        random.nextInt(200), random.nextInt(200));

                tvboxID.setText(macAddress + "");
                tvboxID.bringToFront();
                tvboxID.setLayoutParams(params);
                tvboxID.setVisibility(View.VISIBLE);
                System.out.println("box is shown");

                handlerToHideMac.postDelayed(runnableToHideMac, 1000 * 7);
            }
        };

        handlerToShowMac.postDelayed(runnableToShowMac,5 * 1000 * 60);


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_INFO:
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                controller.show(1000);
                title();
                return true;
            case KeyEvent.KEYCODE_MENU:
                if (listView.getVisibility() == View.VISIBLE) {
                    // Its visible

                    listView.setVisibility(View.INVISIBLE);
                    channelHeadingOption.setVisibility(View.INVISIBLE);

                } else {
                    // Either gone or invisible
                    openMenu();
                }
                return true;
            case KeyEvent.KEYCODE_BACK:
                finish();

//
//                Intent i = new Intent(ChannelPlayActivity.this, MenuActivity.class);
//                i.putExtra("macAddress", macAddress);
//
//                startActivity(i);
                return true;

        }
        return  false;
    }

    private void openMenu() {
        try {

            listView.setVisibility(View.VISIBLE);
            channelHeadingOption.setVisibility(View.VISIBLE);
            channelHeadingOption.setText("MENU");


            relativeLayout.setVisibility(View.VISIBLE);


            String values[] = new String[]{"EPG", "Reconnect", "Re-Launch", "Settings", "About"};
            final ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < values.length; ++i) {
                list.add(values[i]);
            }
            final StableArrayAdapter adapter = new StableArrayAdapter(this,
                    android.R.layout.simple_list_item_1, list);
            listView.setAdapter(adapter);
            listView.requestFocus();
            listView.setSelection(0);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    // ListView Clicked item index
                    int itemPosition = position;

                    // ListView Clicked item value
                    String itemValue = (String) listView.getItemAtPosition(position);


                    switch (itemPosition) {
                        case 0:
//                        new LoadApplications().execute();
                            final CustomDialogManager comingSoon = CustomDialogManager.epg(ChannelPlayActivity.this);
                            comingSoon.setTitle("EPG");
                            comingSoon.setMessage("Will be well worth the wait, EPG Coming Soon!");
                            comingSoon.setNegativeButton("Dismiss", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    comingSoon.dismiss();
                                }
                            });

                            break;
                        case 1:
////        Toast.makeText(this, "OnErrorListener: reconnect status going on", Toast.LENGTH_SHORT).show();
//                       String mac_link = LinkConfig.getString(getApplicationContext(),
//                                LinkConfig.CHANNEL_URL) + "?mc_id=" + macAddress;
//                        System.out.println(mac_link);
//                        new channeljson().execute(mac_link);*/
                            reconnect();
                        /*try {
                            Intent i3 = getPackageManager().getLaunchIntentForPackage(getPackageName());
                            if (i3 != null) {
                                //throw new PackageManager.NameNotFoundException();
                                i3.addCategory(Intent.CATEGORY_LAUNCHER);
                                startActivity(i3);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/

                            listView.setVisibility(View.GONE);
                            relativeLayout.setVisibility(View.GONE);
//                        System.exit(0);
                            //
                            break;
                        case 2:

                            try {
                                Intent i3 = getPackageManager().getLaunchIntentForPackage(getPackageName());
                                if (i3 != null) {
                                    //throw new PackageManager.NameNotFoundException();
                                    i3.addCategory(Intent.CATEGORY_LAUNCHER);
                                    startActivity(i3);
                                }

                            } catch (Exception e) {
                                Logger.printStackTrace(e);
                            }

                            break;
                        case 3:
                            player.reset();

                            Intent intentt = new Intent(
                                    android.provider.Settings.ACTION_SETTINGS);

                            startActivity(intentt);
                            listView.setVisibility(View.GONE);
                            relativeLayout.setVisibility(View.GONE);

                            break;

                        case 4:
                            aboutUs = LinkConfig.getString(
                                    ChannelPlayActivity.this, LinkConfig.ABOUT_US);
                            new aboutDialog().execute(aboutUs);
                            break;


                        default:
                            break;
                    }

                }

            });
            listView.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int i, KeyEvent keyEvent) {

                    if (i == keyEvent.KEYCODE_BACK) {
                        relativeLayout.setVisibility(View.GONE);
                        listView.setVisibility((View.GONE));
                        controller.show();
                        title();


                        return true;
                    }
                    return false;
                }
            });
        } catch (Exception e) {

        }
    }


    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();
        for (ApplicationInfo info : list) {
            try {
                if (null != getPackageManager().getLaunchIntentForPackage(info.packageName)) {
                    applist.add(info);
                }
            } catch (Exception e) {
                Logger.printStackTrace(e);
            }
        }

        return applist;
    }


    private class channeljson extends AsyncTask<String, Void, String> {
        CustomDialogManager loadingChannelLink;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            Log.d("URL",params[0]);

            DownloadUtil dutil = new DownloadUtil(params[0], ChannelPlayActivity.this);
            String json = dutil.downloadStringContent();


            Log.d("TAG", json);
            return json;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(progressBar.isShown()){
                progressBar.setVisibility(View.INVISIBLE);
            }
            Log.d("result", result);
            if (result.equalsIgnoreCase(DownloadUtil.NotOnline) || result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
//                Toast.makeText(ChannelPlayActivity.this,"test",Toast.LENGTH_LONG).show();
                try{
                    if(!noInternet.isShowing()) {
                        try {
                            if (channelDown.isShowing())
                                channelDown.dismiss();
                        }catch (Exception e){}
//                        if(connectingDialog.isShowing())
//                            connectingDialog.hide();
                        noInternet = CustomDialogManager.noInternetDialog(ChannelPlayActivity.this);
                        noInternet.setMessage(getString(R.string.server_unreachable_message));
                        noInternet.getInnerObject().setCancelable(true);
                    }
                }catch(NullPointerException npe){
                    try {
                        if (channelDown.isShowing())
                            channelDown.dismiss();
                    }catch (Exception e){}
                    noInternet = CustomDialogManager.noInternetDialog(ChannelPlayActivity.this);
                    noInternet.setMessage(getString(R.string.server_unreachable_message));
                    noInternet.getInnerObject().setCancelable(true);
                }
                reconnect();




            } else {
                try {
                    if (noInternet.isShowing())
                        noInternet.hide();
                }catch (Exception e){
//                    Toast.makeText(ChannelPlayActivity.this,"catch hide",Toast.LENGTH_LONG).show();
                }

                try {
                    JSONObject jobject = new JSONObject(result);
                    if (jobject.has("link")) {
//                        Toast.makeText(ChannelPlayActivity.this,"link",Toast.LENGTH_LONG).show();

                        channel_url = jobject.getString("link");

                        System.out.println(channel_url + "k ahucha");

//                        onCreate(new Bundle());
//                        Toast.makeText(ChannelPlayActivity.this,"before play",Toast.LENGTH_SHORT).show();
                        playNewVideo(channel_url);
                        progressBar.setVisibility(View.VISIBLE);
//                        checkIfPlaying();

                    } else {
//                        Toast.makeText(ChannelPlayActivity.this,"here",Toast.LENGTH_LONG).show();


                        String errorCode = jobject.getString("error_code");
                        String errorMessage = jobject.getString("error_message");
                        Intent i = new Intent(ChannelPlayActivity.this,
                                UnauthorizedAccess.class);
                        i.putExtra("error_code", "");
                        i.putExtra("error_message", errorMessage + "\n");
                        Log.d("error message", errorMessage);
                        startActivity(i);
                    }


                } catch (JSONException e) {
//                    Toast.makeText(ChannelPlayActivity.this,"catch 2",Toast.LENGTH_LONG).show();

                    final CustomDialogManager dataNotFetched = CustomDialogManager.noInternetDialog(ChannelPlayActivity.this);
                    dataNotFetched.setMessage("Couldn't fetch the Required Data!\n Please Check your Network Connection and contact your service provider");
                    dataNotFetched.setNegativeButton("Re-Launch", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                Intent i = getPackageManager().getLaunchIntentForPackage(getPackageName());
                                if (i != null) {
                                    //throw new PackageManager.NameNotFoundException();
                                    i.addCategory(Intent.CATEGORY_LAUNCHER);
                                    dataNotFetched.dismiss();
                                    startActivity(i);
                                }

                            } catch (Exception e) {
                                Logger.printStackTrace(e);
                            }
                        }
                    });
                }


            }
        }
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }
    }

    private class LoadApplications extends AsyncTask<Void, Void, Void> {
        private List<ApplicationInfo> applist = null;
        private ApplicationAdapter listadaptor = null;
        private CustomDialogManager loading;


        @Override
        protected Void doInBackground(Void... params) {
            applist = checkForLaunchIntent(getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA));
            listadaptor = new ApplicationAdapter(ChannelPlayActivity.this,
                    R.layout.snippet_list_row, applist);

            return null;
        }



        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            loading.dismiss();
            channelHeadingOption.setText("APPLICATIONS");
            listView.setAdapter(listadaptor);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    ApplicationInfo app = applist.get(position);
                    try {
                        Intent intent = getPackageManager()
                                .getLaunchIntentForPackage(app.packageName);

                        if (null != intent) {
                            startActivity(intent);
                        }
                    } catch (ActivityNotFoundException e) {

                    } catch (Exception e) {

                    }
                }
            });




        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = new CustomDialogManager(ChannelPlayActivity.this,CustomDialogManager.LOADING);
            loading.build();
            loading.setExtraButton("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loading.dismiss();
                    cancel(true);
                }
            });
            loading.show();
            loading.getInnerObject().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    if (i==keyEvent.KEYCODE_BACK){
                        loading.dismiss();
                        cancel(true);
                        return true;
                    }
                    return false;
                }
            });
        }


    }
    private void reconnect(){


//        Toast.makeText(ChannelPlayActivity.this,"reconnect call",Toast.LENGTH_LONG).show();
        String mac_link = LinkConfig.getString(getApplicationContext(),
                LinkConfig.CHANNEL_URL);
        new channeljson().execute(mac_link);



    }

    private void checkIfPlaying(){
//        final Handler checkHan = new Handler();
//        final Handler checkHan1 = new Handler();
//        runnableToHideMac = new Runnable() {
//            @Override
//            public void run() {
//                try{
//                    if(!player.isPlaying()){
//                        reconnect();
//                    }
//
//                }catch (Exception e){
//                    reconnect();
//                }
//                checkHan.postDelayed(runnableToShowMac,5000);
//            }
//        };
//        runnableToShowMac = new Runnable() {
//            @Override
//            public void run() {
//                try{
//                    if(!player.isPlaying()){
//                        reconnect();
//                    }
//
//                }catch (Exception e){
//                    reconnect();
//                }
//                checkHan1.postDelayed(runnableToHideMac,5000);
//            }
//        };
//        checkHan.postDelayed(runnableToShowMac,5000);




        TimerMacCheck = new Timer();
        TimerMacCheck.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        try{
                            if(!player.isPlaying()){
                                reconnect();
                            }

                        }catch (Exception e){
                            reconnect();
                        }


                    }


                });
            }

        }, 0, 5000);




    }
    private void showAccountInfoInDialog(){

       /* String mac_link = LinkConfig.getString(getApplicationContext(),
                LinkConfig.ACCOUNT_URL) + "?mc_id=" + macAddress;

        new UserAccountInfoLoader().execute(mac_link);*/

    }

    private class aboutDialog extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            Log.d("ChannelPlayActivity: aboutlink", params[0]);
            DownloadUtil dUtil = new DownloadUtil(params[0], ChannelPlayActivity.this);

            return dUtil.downloadStringContent();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
                final CustomDialogManager noInternet = new CustomDialogManager(ChannelPlayActivity.this, CustomDialogManager.ALERT);
                noInternet.build();
                noInternet.setTitle("No Internet");
                noInternet.setMessage("Please Check Your Internet Connection");
                noInternet.setPositiveButton(getString(R.string.btn_dismiss), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noInternet.dismiss();
                        finish();
                    }
                });
                noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        noInternet.dismiss();
                        startActivity(i);


                    }
                });
                noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(
                                android.provider.Settings.ACTION_SETTINGS);

                        noInternet.dismiss();
                        startActivity(intent);

                    }
                });
                noInternet.show();
                // server not rechable
            } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                Intent i = new Intent(ChannelPlayActivity.this, UnauthorizedAccess.class);
                i.putExtra("error_code", "");
                i.putExtra("error_message",
                        getString(R.string.server_unreachable_message));
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

            } else {
                try {
                    JSONObject jObj = new JSONObject(result);
                    JSONArray jsonMainArr = jObj.getJSONArray("page");
                    for (int i = 0; i < jsonMainArr.length(); i++) {  // **line 2**
                        JSONObject childJSONObject = jsonMainArr.getJSONObject(i);
                        description = childJSONObject.getString("description");
                    }
                    final Dialog d = new Dialog(ChannelPlayActivity.this);
                    d.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    d.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

                    d.getWindow().setLayout(9000,9000);
                    d.setContentView(R.layout.activity_account);
                    TextView aboutus = (TextView) d.findViewById(R.id.aboutus);
                    d.setOnKeyListener(new DialogInterface.OnKeyListener() {
                        @Override
                        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                            if(i==keyEvent.KEYCODE_MENU){
                                d.dismiss();
                                openMenu();
                            }else {
                                d.dismiss();
                            }
                            return true;
                        }
                    });
                    d.show();


                    aboutus.setText(description );


                    System.out.println(description);

//                    boolean updateRequired = jObj.getBoolean("updateRequired");
                }catch (Exception e){

                }
            }

            }
    }
   /* private class  UserAccountInfoLoader extends AsyncTask<String, Void, String > {
        private CustomDialogManager loadingDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = new CustomDialogManager(ChannelPlayActivity.this,CustomDialogManager.LOADING);
            loadingDialog.build();
            loadingDialog.setExtraButton("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadingDialog.dismiss();
                    cancel(true);
                }
            });
            loadingDialog.getInnerObject().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    if(i==keyEvent.KEYCODE_BACK){
                        loadingDialog.dismiss();
                        cancel(true);
                        return true;
                    }
                    return false;
                }
            });
            loadingDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            DownloadUtil dUtil = new DownloadUtil(  params[0], ChannelPlayActivity.this);

            return dUtil.downloadStringContent();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadingDialog.dismiss();
            if (result.equals(DownloadUtil.NotOnline) || result.equals(DownloadUtil.ServerUnrechable)) {
                final CustomDialogManager noInternet = CustomDialogManager.noInternetDialog(ChannelPlayActivity.this);
                noInternet.setNegativeButton("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        noInternet.dismiss();
                        showAccountInfoInDialog();
                    }
                });



            } else{

                try {
                    String userName = "", userEmail = "", phoneNo = "", address = "", logoView = "";
                    JSONArray jArray = new JSONArray(result);
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject jsonobject = jArray.getJSONObject(i);
                        userName = jsonobject.getString("name");
                        userEmail = jsonobject.getString("email_address");
                        phoneNo = jsonobject.getString("phone_no");
                        address = jsonobject.getString("address");
                        logoView = jsonobject.getString("hotel_logo");


                    }

                    final Dialog d = new Dialog(ChannelPlayActivity.this);
                    d.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    d.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

                    d.getWindow().setLayout(8000,4500);
                    d.setContentView(R.layout.activity_account);
                    TextView userEmailView = (TextView) d.findViewById(R.id.emailAddressView);
                    TextView userNameView = (TextView) d.findViewById(R.id.userNameView);
                    TextView phoneNoView = (TextView) d.findViewById(R.id.phoneNoView);
                    TextView addressView = (TextView) d.findViewById(R.id.addressView);
                    ImageView LogoView = (ImageView) d.findViewById(R.id.logoView);
                    TextView boxIdView = (TextView) d.findViewById(R.id.boxIdView);


                    userEmailView.setText(userEmail);
                    boxIdView.setText(macAddress);
                    phoneNoView.setText(phoneNo);
                    userNameView.setText(userName);
                    addressView.setText(address);
                    d.setOnKeyListener(new DialogInterface.OnKeyListener() {
                        @Override
                        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                            if(i==keyEvent.KEYCODE_MENU){
                                d.dismiss();
                                openMenu();
                            }else {
                                d.dismiss();
                            }
                            return true;
                        }
                    });
                    d.show();
//                try {
//                    UrlImageViewHelper.setUrlDrawable(LogoView,
//                            logoView, R.drawable.hoteltv_tvanywhere,
//                            AppConfig.CACHE_HOLD_DURATION);
//                } catch (Exception e) {
//                }


                } catch (JSONException e) {

                    Intent i = new Intent(ChannelPlayActivity.this,
                            UnauthorizedAccess.class);
                    i.putExtra("error_code", "");
                    i.putExtra("error_message", getString(R.string.unauthorized_access_message) + "\n");
                    startActivity(i);

                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }}
    }*/
}
